===========
Data Policy
===========

Working documents related to the ESRF data policy implementation.

Main development website: https://gitlab.esrf.fr/sole/data_policy

Acknowledgements
================

This work is tries to summarize the conclusions derived from discussions with many colleagues.
