ESRF Scans and Sequences
========================

This document intends to provide some light concerning data structuration for scans and sequences.

The documents ESRF_NeXusImplementation.rst (https://gitlab.esrf.fr/sole/data_policy/blob/master/ESRF_NeXusImplementation.rst) and ESRF_NeXusDetector.rst (https://gitlab.esrf.fr/sole/data_policy/blob/master/ESRF_NeXusDetector.rst) should be read in advance.


Introduction
============

The main idea of this document is to present some possible solutions to encountered problems (multiple masters, tomography sequences) from the perspective of where we want to arrive. By that it is to be understood as the ideal (not necessarily utopic) final data layout we would like to achieve so that we provide to the means to achieve that layout at a certain point.


Data Layout
===========

As presented multiple times, there are clear answers about where detector data, motors and counters belong in a simple, traditional, SPEC-like scan (ESRF_NeXusImplementation.rst)

Here a solution is proposed to try to deal with the following cases.

- Multiple masters. Bliss introduced the concept of acquisition with multiple masters to deal with fast and slow channels.
- Sequences. A set of different commands to achieve a certain acquisition.

The multiple masters case
-------------------------

The multiple master case of Bliss can be assimilitated to the SPEC case where one SPEC session was dedicated to collect slow channels and a different one was collecting the fast channels. Seen from that perspective, it is like having two scans sharing the same acquisition command.

It is possible to fit all the acquired data in a single NXentry. However, software dealing with those NXentries can be confused because the dimensionality of the datasets will not match.

The simple solution proposed here is to treat separately the fast and slow channel as like being two different acquisition taken with the same command. In fact, following the above exposed SPEC analogy they are exactely that. How to distinguish a simultaneous acquisition from two separated acquisitions? By introducing the concept of major and minor scan number. Instead of using single numbers like 1, 2, 3, 4... to indicate the scan number, one can use a major and a minor number. That way entries numbered 1.1, 1.2, 1.3, are to be understood as being part of the same acquisition.

Implementation Details. At this point Bliss knows which channels are associated to each master. That information is to be published in Redis so that external writers can deal with the information accordingly.

The sequences
-------------

Sometimes an acquisition follows a set of commands. The reasons can be multiple and we are not going to enter on them. Again, the simplest way to indicate a set of commands are part of the same sequence is to share the same major scan number and to have sequential minor scan numbers. Compared to the standard scan sequences, all what is needed from the scan engine is to provide the user or programmer with the capability to tell the acquisition that a sequence has been initiated in order to prevent the acquisition from incrementing the major scan number.

However, we can illustrate the problem with the tomography acquisitions where it can be difficult to program a single command to deal with dark images

The tomography case
-------------------

Here to look at all the different possible acquisitions from the distances allows to see a common pattern. The reader is invited to take a look at the NeXus application definition for tomography (http://download.nexusformat.org/doc/html/classes/applications/NXtomo.html#nxtomo)

In simple words, any tomography acquisition can be described as nFrames motor positions, nFrames data (images or MCAs) and nFrames flags ("image_keys" in the definition) to indicate how each image or MCA is to be interpreted (0 for projection, 1 for flat, 2 for dark, 3 for invalid).

Can we afford to acquire the data directly like that with a single command? It might be but it is certainly simpler at this point to use a set of commands to achieve the same goal. What is critical is to keep in mind the structure we would like to achieve. If each of the intermediate commands is treated as a standard acquisition, all what is needed is to indicate that they are part of the same sequence. At any moment (perhaps at the end of the sequence) it should be straightforward to start an external process that takes the different pieces and creates the appropriate layout via a Virtual Dataset. 

Keeping that in mind, it is also critical that generated images can be associated to scans. If data from each detector is going to land in a separate directory, the old fashion naming using three digits to indicate SCANNUMBER_DETECTORNUMBER_FILEINDEX can be recovered to indicate SCANMAJOR_SCANMINOR_FILEINDEX.

Conclusion
==========


The old fashion approach of using two indices for the scans can find multiple applications. If this solution is retained, it would be desirable for the bliss scan engine to foresee a major scan number that can be blocked to indicate a sequence and a minor scan index always incrementing and resetted to one when the major scan index changes.


