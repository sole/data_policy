=======================================
ESRF Generic Data Policy Implementation
=======================================

Introduction
============

With the decision to implement a data policy, the ESRF has taken a clear step forward to give appropriate value to data collected during the experiments. While some of the objectives are clear (data archival, public access) their implementation present challenges at almost every ESRF level.

Just to have years of archived data publicly available on a self-service like repository would be almost useless. The added value for the scientific community of the ESRF huge data implementation effort would be limited because the data policy implementation would be reduced to a very expensive backup. Basically only the proposers of a particular experiment would be able to make use of the data collected during the experiment.

However, if generated data have associated digital object identifiers, publications can refer to them through that unique identifier and a database should be able to map that identifier to the relevant files associated to the publications. The main interest to the scientific community would be the reproducibility of results. While that is already an important goal on its own, the ESRF data policy implementation aims to go beyond that. If data are properly described, the database can be used to perform data mining. Searches like "search for all EXAFS experiments of Pt L3 edge" should be possible at a certain point with evident impact for the scientific community.

From a practical point of view, to develop an implementation strategy based on each type of experiment is not convenient. Only in case of multiple beamlines performing same type of experiments and with clear standardization conventions that approach can be envisaged. The approach based on a generic implementation that can be later on made more specific to particular techniques looks more promising.

This document covers the details of the data policy implementation related to interaction between data acquisition and the database. The details concerning data acquisition and the raw data information and readability can be found detailed in accompanying document (ESRF Nexus Implementation). 

Goals of the Generic Implementation
===================================

The generic solution presented here aims to allow a quick deployment of the data policy at the ESRF. It is based on storing relevant information that is common to most beamlines and on a data layout that prepares the data to be archived.
It is our understanding that at the very minimum a generic solution should allow performing searches by proposal, beamline, user, sample and technique. Anything else is to be considered as a bonus. 
The ICAT database

In line with other facilities, we have chosen ICAT (https://icatproject.org) as database. One thing to be kept in mind is that the role of the database is to be able to provide access to the data according to different search criteria (aka. \"metadata\"). The role of the database is not to store the data themselves. The role of the database is the same of a database for bibliography search but applied to data. In one case one specify keywords to find publications and to go further one gets the publication themselves. The role of ICAT is analogous. We aim to find the data associated to a set of criteria and to go further one has to dig into the data themselves.
The information that can be stored in ICAT has the form key = value, with key being a string and value being either a single string or a single number.
Besides all the information being accessible from the database, an ESRF NeXus HDF5 file containing a copy of that information is generated at the beamline. That file is going to be referred to as the master file. The idea is to have all the data accessible from that file besides having them accessible from the database. Furthermore, the master file can be used to store beamline specific metadata that have little or no interest on being on a common database.

Database Key conventions
========================

While the master file can make use of the hierarchical capabilities of the underlying physical format, the database is limited to store the information using strings. We face the problem of name collisions with commonly used metadata. For instance: energy can refer to the synchrotron electrons, to the incident photon beam or to the outgoing beam; description or name can refer to a multitude of items.

For all the metadata for which there is a NeXus equivalent, we are going to use an approach based on it. NeXus stores the different parameters in groups named classes. For example the sample name and description are stored in a class named NXsample. We are going to translate that into a Sample_name and Sample_description. For the NXmonochromator group, that it is inside the NXinstrument group, the different parameters will be stored in keys of the form InstrumentMonochromator_parameter. Relevant attributes will be added after a dot instead of using the \"@\" symbol to avoid problems in the description files.
In other words, the hierarchy of containers is going to be stored using CamelCase convention while the parameters using the lower_case_with_underscores convention.

Organization of Acquisition Directories
=======================================

In order to pave their way towards the final archival, the files created during acquisition of a particular proposal will be stored following a path root that maps to the unique identifier of the data in the database. The proposed path root is:

/PROPOSAL/BEAMLINE/NAME/ICAT_IDENTIFIER/

In the case where SPEC is used as sequencer, the simplest implementation is to replace the newfile macro by a newsample macro that requires additional parameters::

  FOURC> newsample
         Sample name?
         Sample description?
         Techniques?
         File?

Sample name is a string without spaces that is going to be mapped to the NAME in our path root.
Sample description is a long string (minimum length to be forced?) describing the sample or purpose of the entry that is going to be created in the ICAT database.
Techniques should be a list of comma separated strings specifying recognized database techniques (KMAP, CDI, XRF, XANES, TOMO, FLUOTOMO, …) or purposes (ALIGNMENT, COMMISSIONING, …)
File is the filename (with eventual additional path) to be appended to the automatically generated path root.

For the purpose of data storage the Sample description and the Techniques questions are not necessary, but they should provide huge added value when performing searches at a database.

Let’s say the user has specified SAMPLE_A and file_000.dat as sample name and file respectively. The actual complete path of the generated data would be:

/PROPOSAL/BEAMLINE/SAMPLE_A/ICAT_IDENTIFIER/FOURC/file_000.dat

The “magical” FOURC illustrates the approach that is going to be generic. Each file generator (SPEC, device servers, ) will place its data into its own directory inside the generated path root. For that, every acquisition macro and device server will have to be provided with the acquisition path to be used.
Processed Data

Files containing processed data generated during the experiment or analysis configuration files used during that time can be stored inside a PROCESS folder:

/PROPOSAL/BEAMLINE/SAMPLE_A/ICAT_IDENTIFIER/PROCESS

At this moment there is no warranty about when the content of that folder will be archived. In other words, if the dataset is already archived, further treatment files added to that folder will not be archived. It is a decision to be taken, how long after the end of the collection dataset, the dataset is archived.

Specific Techniques and Existing Solutions
==========================================

This solution, while being generic, is not restrictive at all. One can imagine that in the future, following the selected techniques, other parameters will become mandatory (chemical element and edge for absorption spectroscopy, protein for protein crystallography). Graphical user interfaces (GUIs) can be envisaged.

As a matter of fact, some beamlines had specified the set of metadata and names they wanted to storage. An extension mechanism that merges NeXus and ICAT has been envisaged. For each ICAT identifier we can have an equivalent entry in the HDF5 master file. There we are going to make use of the “definition” keyword to store the list of techniques associated to the ICAT identifier. Once the specific information related to the technique will be there, we should find it under set of keywords combining the words Subentry and the technique.

An example should help to clarify things. Let’s say we are measuring SAXS and WAXS data for which all the required metadata have been specified. The “definition” would contain the sentence “SAXS, WAXS” and we would have the SubentrySAXS\_ and SubentryWAXS\_ containers that could precede the metadata parameters following our convention.

Existing solutions, ftomo, mrt, just need to be renamed to SubentryFtomo and SubentryMrt or something similar to follow the guidelines presented here.

An updated list of all the defined keys in the database can be found at:

https://gitlab.esrf.fr/icat/hdf5-master-config/blob/master/hdf5_cfg.xml

Electronic Logbook
==================

It would be desirable for the users to be able to access the logbook of their experiments. A quick look at the control room logbook shows that minimalistic and simple information can be very useful.
A very basic information could consist on storing information corresponding to the fields PROPOSAL, DATASET, TIMESTAMP, EVENT, DESCRIPTION and HARDCOPY.

- PROPOSAL, DATASET AND TIMESTAMP are self-explanatory.
- EVENT could take values like COMMAND, COMMENT, ERROR, START_DATASET, END_DATASET,
- DESCRIPTION would be a text containing the command, comment,... 
- HARDCOPY would contain the path to files (snapshots, pdfs,...)

Most of those fields could be automatically filled.

Such a basic implementation would allow to export the logbook from a database to an HDF5 in the form of NXnotes if desired.

The electronic logbook should be available without having to access the tapes. It should be in "live storage". 

SPEC and HDF5
=============

SPEC can generate HDF5. However, it would be at the very least tedious to modify every acquisition macro to generate HDF5 files. It is much simpler and practical to convert SPEC generated files to HDF5. That can be achieved in a transparent manner by implementing the appropriate code in the prompt_mac macro. That macro is automatically called each time the prompt is reached therefore the HDF5 file could be automatically updated.

As an added bonus, the master file that contains a mirror of the information stored in ICAT could keep an external link to that “SPEC HDF5” file. That would allow us to reach other goal: from the master file we would be able to reach actual data.

Generic macros will be provided. These macros will take care of handling common parameters. Beamlines will be able to extend/customize those macros via a generic commad allowing to set particular keys. Only keys defined in ICAT will be allowed. In order to give the opportunity to store custom information, a maximum of 10 notes per ICAT identifier will be allowed. These notes take the form of long text strings (preferably time stamped).

Summary
=======
The presented solution aims to be simple and easy to deploy. The effort at the acquisition system is reduced to a minimum. The most tedious part will be to modify the file name generation to take into account a path root. It is realistic to say that beamlines using standard macros could implement the data policy in a very short time after the validation tests.

At the beamline and user scientist side, they have to get used to a new command that in short term they are going to judge useless and cumbersome but that is certainly going to pay off. Initial tests show that if the proposed new approach is kept in parallel with the ancient one, the new approach is abandoned almost immediately. The new approach should fully replace the ancient one. In other words, to come back to the ancient approach should be way more cumbersome than just to type two sentences.

