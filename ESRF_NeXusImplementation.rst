=========================
ESRF NeXus Implementation
=========================

Introduction
============

Among the different steps involved in the implementation of the ESRF data policy, there is the subject of the data file format. Indeed the use of proprietary formats has almost no sense when talking about making data freely available. In that sense, a data format allowing to store any kind of data in a portable manner regarding operating systems, hardware or computer language, it is a must.

The hierarchical data format (HDF5) completely fulfils those requirements and it is the solution adopted by most European Synchrotrons (SOLEIL, DIAMOND, DESY, �)

HDF5
====

This file format is the exact analogue of a portable file system. In other words, it behaves like a portable hard disk or USB stick. Any type of data can be stored in it and the data can be structured by means of Groups, Datasets, Attributes and Links.

- Groups are just containers. In our �hard disk� analogy they are the equivalent of folders/directories.

- Datasets are the actual data. In our analogy they are the equivalent of files.

- Attributes are just short tags of the form �key = value� that can attached to any dataset or group. In our analogy, they play the role of the information we can obtain form files or folders when we look at its properties (information accessible via a right click for instance).

- Links are the equivalent of hard disk links. They allow to access the same groups or data in different ways without the need of duplicating the data.

Figure 1 illustrates how datasets can be accessed. One has to provide the path to the dataset following the equivalent of its path. It is worth to be noted that links can go from one HDF5 file to another one.

.. image:: ESRF_NeXusImplementation_00.png

The use of HDF5 warrants accessibility to the data handling things like data type, dimension, endianness and compression. It does not tell anything about if the datasets are the output of a detector or the different positions of a motor. Furthermore, the user is free to organize the data inside the file  in any way. In our hard disk analogy, the user is allowed to place the files anywhere, inside or outside folders. These type of problems are addressed by the NeXus convention.
 
  
NeXus
=====

NeXus is an initiative trying to facilitate the exchange of data and analysis tools. As such it provides a set of principles to help people understand what is in the files, an application programmers interface (API) to help programmers read and write those files and a forum for discussion.

The NeXus API supports different physical file formats (HDF4, XML, HDF5, �) From all of them, the only one that is adapted to synchrotron data and widely adopted is HDF5. The fact the NeXus API is less robust than the HDF5 API and the fact that, in addition, it follows HDF5 evolutions at a much slower pace, has led to most labs to ignore the NeXus API and to directly write HDF5 files with other tools while keeping use of the NeXus convention.

Because of that, we are going to refer to NeXus as a convention while we are going to refer to HDF5 as a data format.

In the NeXus convention data are organized in Groups, Fields, Attributes and Links. Since the role of the fields is the same of that of datasets, we see that there is a one to one map of NeXus to HDF5.

In the NeXus convention, the expected contents of a Group are determined by the value of the NX_class attribute associated to that Group. The name of the Group itself is left free to the user. There are however limitations (one cannot have the �/� character as name anywhere to avoid path conflicts) and recommendations (not to use spaces in the names). 

There are more than 50 �classes� defined. See for instance:

http://download.nexusformat.org/doc/html/classes/base_classes/index.html

Only a few of them will be presented here because they govern the overall structure of the file. In what follows we are going to represent attributes by the @ symbol.

The top level of the file, the �/� level, is referred to as NXroot (@NX_class = �NXroot�). It just defines a set of attributes besides @NX_class. Those attributes take care of what was the original name of the file when created, last time it was updated,� Since we are only interested on the structure of the file, we are not going to enter into details.

The top level Group �/� (@NX_class = �NXroot�) can *only* contain other Groups with attribute @NX_class = �NXentry�. The typical structure follows::

  entry_name (@NX_class=NXentry, @default=plot_data_name)
       title
       start_time
       end_time
       instrument_name(@NX_class=NXinstrument)
                   detector0_name(@NX_class=NXdetector)
                                  �.
                   positioner0_name(@NX_class=NXpositioner)
                                  �.
       user_group_name(@NX_class=NXuser)
                   user name, affiliation, �
       plot_data_name(@NX_class=NXdata)
                   contains information defining a plot
       subentry_name(@NX_class=NXsubentry)
                   definition (string specifying the interpretation of the informationx for analysis purposes)
                   required_analysis_data0
                   �
                   required_analysis_datan      

In that structure we find the basis of NeXus files. There is a single NXinstrument group containing all the description of the beamline, the different detector used and their data, the different motors (positioners in NeXus jargon) and their value. A single group providing user (NXuser) and a single group providing information about the sample (NXsample information).

The NXdata group, despite its name, contains a plot. It provides everything needed to generate the plot (datasets to be used as axes values, datasets to be represented agains the axes values, error bars, labels, �) There is no limit on the number of NXdata groups present nor in its location within the NXentry. It is even considered good practice that each detector (NXdetector) provides its NXdata group to visualize its data.

Previous groups allow to organize the data in the file, but still do not provide any interpretation about what they represent thus not allowing portable analysis. In order to solve that issue, NeXus introduced the concept of application definitions.  Those are well defined, named-based ways of specifying the required data for a particular analysis. NeXus offers two ways to specify that information but only one of them allows to handle experiments using simultaneously different techniques (ex. combined fluorescence and diffraction, SAXS and WAXS, �) At the ESRF we are going to retain only the most modern approach that is based on the use of groups of type NXsubentry.

The NXsubentry groups, if any, are found at the hierarchical level just below NXentry. Its name is free but it is of common sense to set it to something letting the user know the type of experiment described without going any further. The key information inside the NXsubentry is carried out by what in NeXus jargon is known as an application definition. It is mandatory to have a dataset of type string *and name* definition set to the implemented definition. That dataset has the attributes @version and @URL specifying the implemented version of the definition and the URL where to find the description of what it implements. For instance, let�s say we are interested on absorption spectroscopy and that for that we need to have the datasets energy, I0 and It, of float type corresponding to the beam energy, the intensity of the incident beam and the intensity of the transmitted beam. Let�s also assume we are going to name that definition simpleXAS. The implementation of that definition would imply:


  #. To create an NXsubentry group containing a string dataset named definition set to �simpleXAS� with the (optional but strongly recommended) attributes @version=�1.0� and @URL set to a web page where to find a complete description of all the mandatory and optional items found. 
  #. To have a dataset named energy, or a link named energy to a dataset with any name but of type float.
  #. To have a dataset named I0, or a link named I0 to a dataset with any name but of type float.
  #. To have a dataset named It, or a link named It to a dataset with any name but of type float.

That implementation would warrant that any analysis code, claiming to support our simpleXAS definition, should be able to analyse and understand the data irrespectively of the hardware of facility used to carry out the experiment.

Limitations
===========

We have solved the storage problem, the data access problem and the data analysis problem. What is not solved is the diagnostics problem. When everything goes well, the default plot might be all what a beamline scientist needs to look at. However, experience shows that beamline scientists need to be able to visualize anything against almost anything. In that sense, the hierarchy proposed by NeXus is overkill and cumbersome. Imagine to have to browse three different directories each time you want to retrieve a motor, a counter and a normalizing counter just to get a plot. That is using a graphical user interface (GUI). If we are talking about interactive use, it is unbearable.

To solve that issue, ESRF staff proposed to create an additional group, named �measurement� containing everything the scientists need for quick diagnostics. That approach has been retained by other synchrotrons and it is followed by the Sardana acquisition system. The idea is to have everything measured in a single group with as little structure as possible. The typical use would be to put there all the motors that have moved during a measurement, all the counters, all the detector data and so on. If anything additional is needed (for instance pixel size associated to a dataset generated by a pixel detector), that information is to be found in the NXinstrument group.
 
Since data is already a member of the NXdetector group, the simplest implementation of it would be mca_device0 to be a link to the NXdetector group containing the detector and where data and, eventually, info would be found. One would expect the beamline scientist to be able to give an alias to the detector (for instance saxs_det) and to use that alias as a link to the NXdetector group inside NXinstrument that would typically use the device server name.

To simplify diagnostics, we should have a group named positioners containing the values of all the motor positions of the instrument. Scanned motors would have as many values as scan points while fixed motors would only have one value. Anything that can be scanned in a beamline (the temperature of a temperature controller) can have its place there. After discussion with BCU people, it was agreed to have that group inside the NXinstrument group and not inside the measurement group.

The overall structure of the file follows.

.. image:: ESRF_NeXusImplementation_01.png

That structure is the basis of ESRF generated NeXus files. Keeping with the philosophy detailed in this document, any particularity, like multiple timers, event based data, does not change anything. If the data are generated by multiple timers, they are event based or whatever, they follow the same approach. They are measured data, they appear in the measurement group and in the detector group to which belong. Any details about the conditions of the acquisition have to go in the NXinstrument group. Cases foreseen by NeXus will be handled following NeXus. Cases not handled by NeXus will be implemented at ESRF and proposed to the NeXus committee for acceptation as NeXus standard.

Although the control system has to fill most, if not all, that information, one can easily appreciate that the red part is to be filled by BCU with very little intervention of the beamline scientists or users. Basically the only customization to be offered to the scientists is the eventual aliases to be used for the detectors requiring a link. The green part is mostly decided by the user (the data he was visualizing, the sample he was using, �) and the blue part should be recovered according to the proposal.

The purely analysis part, contained in NXsubentry, can only be generated at acquisition time if the user has clearly specified the type of experiment he is carrying (ex. the definition used) and clear instructions are given to BCU about how to map the definition to the measured data.

Processed Data
==============

Our goal is to store a maximum of information and to minimize the number of files. All that can be achieved creating an HDF5 group with information related to each data processing step in which it is critical to have

-	Program used
-	Configuration paramters
-	Results
-	Order in the data treatment sequence

In the NeXus convention this can be achieved by using a slightly extended NXprocess group::

  entry/
  entry/start_time
  entry/end_time
  entry/title
  entry/process_name@NXprocess
  entry/process_name/program
  entry/process_name/version
  entry/process_name/date
  entry/process_name/sequence_index
  entry/process_name/configuration@NXcollection if the analysis code understands HDF5
  entry/process_name/configuration@format=�ini� or whatever � if string
  entry/process_name/results@NXcollection or NXdata if plot

The extension to the original NeXus NXprocess description is the configuration and results fields. Probably this can be brought closer to NeXus conventions by adding some of the concepts of the NXnote class like file_name and type (containing Mime content type) as attributes of the configuration entry. This is particularly interesting in the case the configuration consists on a binary representation of a file understood by the analysis program.

Summary
=======

More than ever it is clear that it is not enough just to write the data in a portable manner.

Data have to be written keeping in mind how they will be analysed. DAU people will work in close collaboration with and help BCU people on interpreting, applying and verifying the different conventions.

To simplify use and to reduce entropy, the NXuser group should be named user and the NXsample group should be named sample.

Whenever possible, the default plot should be automatically generated by the sequencer based on what the user was watching when collecting data.  

Technique specific fields will require, prior to implement them, an agreement by scientists and/or scientific communities and/or analysis codes developers about the mandatory information needed to perform the analysis.


