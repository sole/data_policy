ESRF NeXus Detector
===================

Acquisition of images (via LImA), MCA or basically any detector ends up on having an entry on an HDF5 file formatted according to the NeXus conventions. The minimal, common, information that should be there is:

entry_name@NX_class=NXentry
    title
    start_time
    end_time
    instrument_name@NX_class=NXinstrument
         detector_name@NX_class=NXdetector
              data@interpretation=image for 2D data, spectrum for 1D data, nothing or scalar (or scaler) for the counters.

Acquisition is free to add any other field inside the NXdetector group as far as its meaning does not conflict with any field already defined by NeXus.


The MCA case
============

Ideally an MCA detector can provide additional information like the channels, the elapsed_time, the preset_time, the live_time and the calibration. Most of the parameters are likely known by the detector controller (preset_time, live_time, elapsed_time) while the calibration (if any) might only be known by bliss.

Case 1 - File written by controller
-----------------------------------

In this case the NeXus file is written by the MCA controller/device server as LImA is currently doing. That forces that any additional information (like the calibration) is sent to the the device server for addition to the file.

The external writer to be developed by DAU would aim at writing external links to that file in analogy to what is going to be with LImA.

Case 2 - File written by external writer
----------------------------------------

In this case the MCA controler/device has to write the information to memcached from where the external writer will fetch it. That allows to keep MCA data (from memcached), counters and motors (from REDIS) inside the same file.

Why two approaches?
-------------------

It is clearly desirable to keep together counters, motors and MCA data. The contrary would be a step back compared to SPECfile. The only reasons to implement the Case 1 are

- to keep the full analogy between LImA and MCA data and 
- to gain experience at our side with using external links and virtual datasets since LImA is not foreseen to use the memcached solution prior to the restart.


